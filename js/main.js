//console.log("Hello World");

$(function() {

    //переключатель меню

    $('.tabs-menu__list').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.tabs').find('.tabs-gallery').removeClass('active').eq($(this).index()).addClass('active');
    });


    //popup-gallery

    $('.image-popup-zoom').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',

    });

    //slick-slider

    $('.main-banner-slider').slick({
        slidesToShow: 1,
        autoplay: true,
        arrows: false,
        fade: true,
        zIndex: -2
    });

    $('.testimonials-slider').slick({
        slidesToShow: 1,
        autoplay: true
    });

    //переходы (якоря)

    $('a[href^="#"]').bind('click.smothscroll', function() {
        var target = $(this).attr('href'),
            bl_top = $(target).offset().top;
        $('body, html').animate({scrollTop: bl_top}, 700);
        return false;
    });

    //button--fixed

    var top_show = 150;
    var delay = 250;
    $(document).ready(function() {
        $(window).scroll(function () {
            if ($(this).scrollTop() > top_show) $('.button--fixed').fadeIn(200);
            else $('.button--fixed').fadeOut(200);
        });
        $('.button--fixed').click(function () {
            $('body, html').animate({
                scrollTop: 0
            }, delay);
        });
    });

});


